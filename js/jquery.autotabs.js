/*!
 * jQuery autotabs Plugin
 * version 0.0.2
 * @requires jQuery v1.3.2 or later
 * Copyright (c) 2010 Keywan Ghadami (ibson.com)
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 */
;(function($) {
  $.fn.title_tabs = function(options) {

	return this.each(function() {
		var tabs = $(this);

		var ul = $('<ul></ul>');
		tabs.prepend(ul);
		tabs.find('.tab').each(function(index){
			var legend = $(this).attr('title');
			if (legend == ''){
				legend = 'tab_' + index;
			}
			var id = $(this).attr('id');
			if (id == ''){
				id = 'tab_' + index;
				$(this).attr("id",id);
			}
			var tab_code = '<li><a href="#' + id + '">'+legend+'</a></li>';
			ul.append(tab_code);
		});
		return tabs.tabs(options);  
	});
	};
  
	$.fn.fieldset_tabs = function(options) {
		return this.each(function() {
			var tabs = $(this);
			var $ul = $('<ul></ul>');
			tabs.prepend($ul);
			tabs.find('fieldset').each(function(index){
				var legend = $(this).find('legend').text();
				$(this).find('legend').remove();
				var id = 'tab_' + index;
				$(this).attr("id",id);
				var tab_code = '<li><a href="#' + id + '">'+legend+'</a></li>';
				$ul.append(tab_code);
			});
			return tabs.tabs();
		});
	}
   
})( jQuery );